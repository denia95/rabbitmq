﻿using System.Threading;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            RabbitMQService service = new RabbitMQService("PingPongExchange", "direct", "ping_key" , "pong_key", "pong_queue");

            while (true)
            {
                service.ListenQueue();
                Thread.Sleep(2500);
                service.SendMessageToQueue("Pong");
            }

            service.Dispose();
        }
    }
}
