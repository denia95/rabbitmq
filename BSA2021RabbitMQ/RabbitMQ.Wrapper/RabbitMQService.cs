﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class RabbitMQService
    {
        public IConnection producerConnection;
        public IModel producerChannel;

        public IConnection consumerConnection;
        public IModel consumerChannel;

        private MessageProducer _messageProducer;
        private MessageConsumer _messageConsumer;

        public RabbitMQService(string exchangeName, string exchangeType, string producerRoutKey, string consumerRoutKey, string queueName)
        {
            _messageProducer = new MessageProducer(exchangeName, exchangeType, producerRoutKey);
            _messageConsumer = new MessageConsumer(exchangeName, exchangeType, consumerRoutKey, queueName);
        }

        public void SendMessageToQueue(string message)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };

            producerConnection = factory.CreateConnection();
            producerChannel = producerConnection.CreateModel();
            {
                producerChannel.ExchangeDeclare(_messageProducer.ExchangeName, _messageProducer.ExchangeType);

                var body = Encoding.UTF8.GetBytes(message);

                producerChannel.BasicPublish(exchange: _messageProducer.ExchangeName,
                                     routingKey: _messageProducer.RoutingKeyName,
                                     basicProperties: null,
                                     body: body);
            }
        }

        public void ListenQueue()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
       
            consumerConnection = factory.CreateConnection();
            consumerChannel = consumerConnection.CreateModel();

            consumerChannel.ExchangeDeclare(_messageConsumer.ExchangeName, _messageConsumer.ExchangeType);
            consumerChannel.QueueDeclare(queue: _messageConsumer.QueueName,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            consumerChannel.QueueBind(_messageConsumer.QueueName, _messageConsumer.ExchangeName, _messageConsumer.RoutingKeyName);

                var consumer = new EventingBasicConsumer(consumerChannel);
                consumer.Received += (sender, ea) =>
                {
                    var message = Encoding.UTF8.GetString(ea.Body.ToArray());
                    
                    Console.WriteLine(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " " +  message);

                    consumerChannel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                };

            consumerChannel.BasicConsume(queue: _messageConsumer.QueueName,
                                     autoAck: true,
                                     consumer: consumer);
        }

        public void Dispose()
        {
            producerConnection?.Dispose();
            producerChannel?.Dispose();
            consumerConnection?.Dispose();
            consumerChannel?.Dispose();
        }
    }   
}
