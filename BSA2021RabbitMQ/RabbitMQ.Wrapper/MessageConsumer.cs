﻿namespace RabbitMQ.Wrapper
{
    class MessageConsumer
    {
        public string ExchangeName { get; set; }
        public string ExchangeType { get; set; }
        public string RoutingKeyName { get; set; }
        public string QueueName { get; set; }

        public MessageConsumer(string exchangeName, string exchangeType, string consumerRoutKey, string queueName)
        {
            ExchangeName = exchangeName;
            ExchangeType = exchangeType;
            RoutingKeyName = consumerRoutKey;
            QueueName = queueName;
        }
    }
}
