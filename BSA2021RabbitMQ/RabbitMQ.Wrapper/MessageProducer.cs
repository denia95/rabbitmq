﻿namespace RabbitMQ.Wrapper
{
    class MessageProducer
    {
        public string ExchangeName { get; set; }
        public string ExchangeType { get; set; }
        public string RoutingKeyName { get; set; }

        public MessageProducer(string exchangeName, string exchangeType, string producerRoutKey)
        {
            ExchangeName = exchangeName;
            ExchangeType = exchangeType;
            RoutingKeyName = producerRoutKey;
        }
    }
}
