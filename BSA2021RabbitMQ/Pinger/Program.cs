﻿using System.Threading;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            RabbitMQService service = new RabbitMQService("PingPongExchange", "direct", "pong_key","ping_key", "ping_queue");

            while (true)
            {
                service.SendMessageToQueue("Ping");
                Thread.Sleep(2500);
                service.ListenQueue();
            }

            service.Dispose();
        }
    }
}
